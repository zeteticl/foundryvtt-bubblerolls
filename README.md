# Bubble Rolls
This FoundryVTT Module will display Actor Rolls in Chat Bubbles.  

![BubbleRolls demo](demo.gif)

## Setup
To install this module, go to the World Configuration and Setup, Addon Modules, Install Module.  
Then you may copy this url `https://gitlab.com/mesfoliesludiques/foundryvtt-bubblerolls/-/raw/master/bubblerolls/module.json`

In the Module Settings you can select between several templates to change the appearance of the Bubble.

## Adding new templates
You can add new templates pretty easily.  
Templates have access to the ChatMessage object so you have a lot of infos to work with.

To create a new template you must put a new html file in the templates folder, then you must edit the config.js file to include it in the settings.  
It is recommended to use localized string, so you will have to update the lang/*.json with the wanted template name.

## Contributions
Every contribution is welcome, feel free to add templates, translations, or even improve the core module.
